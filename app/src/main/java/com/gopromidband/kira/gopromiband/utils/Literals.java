package com.gopromidband.kira.gopromiband.utils;

/**
 * Created by kira on 2015-10-04.
 */
public class Literals {

    public static class BLUETOOTHLE{
        public static int REQUEST_ENABLE_BT = 1221;
        public static final long SCAN_PERIOD = 10000;
    }
}
