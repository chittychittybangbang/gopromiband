package com.gopromidband.kira.gopromiband;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.gopromidband.kira.gopromiband.utils.Literals;

import java.util.ArrayList;
import java.util.List;

@TargetApi(22)
public class MainActivity extends AppCompatActivity {

    private BluetoothAdapter _bluetoothAdapter;
    private Handler _Handler;

    private BluetoothLeScanner _LEScanner;
    private ScanCallback _LEScanCallback;
    private ScanSettings _LEScanSettings;
    private List<ScanFilter> _LEScanFilters;

    private BluetoothGatt _Gatt;
    private final BluetoothGattCallback _gattCallback;

    {
        _LEScanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                Log.i("CallBackType", String.valueOf(callbackType));
                Log.i("result",result.toString());
                BluetoothDevice device = result.getDevice();
                connectToDevice(device);
            }

            @Override
            public void onBatchScanResults(List<ScanResult> results) {
                for(ScanResult sr: results){
                    Log.i("ScanResult - Results",sr.toString());
                }
            }

            @Override
            public void onScanFailed(int errorCode) {

                Log.e("Scan Failed", "errorCode"+errorCode);
            }
        };

        _gattCallback= new BluetoothGattCallback() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                Log.i("onConnectionStateChange","Status="+status);
                switch(newState){
                    case BluetoothProfile.STATE_CONNECTED:
                        Log.i("gattCallback","STATE CONNECTED");
                        _Gatt.discoverServices();
                        break;
                    case BluetoothProfile.STATE_DISCONNECTED:
                        Log.e("gattCallback","STATE DISCONNECTED");
                        break;
                    default:
                        Log.e("gattCallback","STATE OTHER");
                }

            }
            @Override
            public void onServicesDiscovered(BluetoothGatt _gatt, int status){
                List<BluetoothGattService> services = _gatt.getServices();
                Log.i("onServiceDiscovered", services.toString());
                _gatt.readCharacteristic(services.get(1).getCharacteristics().get(0));

            }

            @Override
            public void onCharacteristicRead(BluetoothGatt _gatt,
                                             BluetoothGattCharacteristic characteristics,
                                             int status){
                Log.i("onCharacteristicRead",characteristics.toString());
                _gatt.disconnect();
            }

        };
    }

    private void connectToDevice(BluetoothDevice device) {
        if(_Gatt==null){
            _Gatt=device.connectGatt(this,false,_gattCallback);
            scanLeDevice(false);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _Handler = new Handler();

        _bluetoothAdapter = ((BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE))
                .getAdapter();


    }

    @Override
    protected void onResume(){
        super.onResume();
        if (_bluetoothAdapter== null || !_bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, Literals.BLUETOOTHLE.REQUEST_ENABLE_BT);
        }else{
            _LEScanner= _bluetoothAdapter.getBluetoothLeScanner();
            _LEScanSettings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            _LEScanFilters= new ArrayList<ScanFilter>();

        }
        scanLeDevice(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (_bluetoothAdapter == null || !_bluetoothAdapter.isEnabled()) {
            scanLeDevice(false);
        }

    }

    @Override
    protected void onDestroy(){

        if(_Gatt == null){
            return;
        }
        _Gatt.close();
        _Gatt=null;
        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode== Literals.BLUETOOTHLE.REQUEST_ENABLE_BT){
            if(resultCode == Activity.RESULT_CANCELED){
                finish();
                //bluetooth no enabled
                return;
            }
        }
        super.onActivityResult(requestCode,resultCode,data);
    }




    private void scanLeDevice(final boolean enable) {

        if (enable) {
            // Stops scanning after a pre-defined scan period.
            _Handler.postDelayed(new Runnable(){
                @Override
                public void run(){
                    _LEScanner.stopScan(_LEScanCallback);
                }
            },Literals.BLUETOOTHLE.SCAN_PERIOD);;
            _LEScanner.startScan(_LEScanFilters, _LEScanSettings, _LEScanCallback);
        } else {
            _LEScanner.stopScan(_LEScanCallback);
        }
    }
}
